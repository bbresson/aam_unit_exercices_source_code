#import "imports/conf.typ": conf
#import "imports/colorbox.typ": colorbox
#import "@preview/diagraph:0.2.1": *
#import "@preview/quick-maths:0.1.0": shorthands
#import "@preview/drafting:0.2.0" : *
#import "@preview/colorful-boxes:1.1.0": stickybox
#let tab = context {
  let tab_left = 2.5cm // leftmost tab stop (i.e. page margin)
  let tab_width = 2.0cm // width of a single tab stop

  let pos = here().position() // caller position
  let column = 1 + calc.trunc( (pos.x - tab_left) / tab_width )   // column where this tab is located
  let advance_to = tab_left + column * tab_width   // position needed for the next tab stop

  // create box with the required width
  // the box requires visible content to actually occupy the correct width
  // for debug purposes we choose the computed column as content
  box(width: advance_to - pos.x)[#text(size: 8pt, fill: purple)[ ]]
}

#set page(
  margin: auto, paper: "a4"
)
#set-page-properties()

#let default-rect(stroke: none, fill: none, width: 0pt, content) = {
  set text(0.9em)
  stickybox(rotation: 30deg, width: width/1.5, content)
}
#set-margin-note-defaults(rect: default-rect, stroke: none, side: right)


#show: shorthands.with(
  ($+-$, $plus.minus$),
  ($|-$, math.tack),
)

#let rainbow(content) = { // for rainbow text
  set text(fill: gradient.linear(..color.map.rainbow))
  box(content)
}

#let centered_rainbow_box() = { // for rainbow box
  align(center)[#rect(width: 350pt, height: 70pt, inset: 8pt, stroke: 3pt + gradient.linear(..color.map.rainbow))[ ]]
}


// #set par(first-line-indent: 1em, justify: true)
#set par(justify: true)

#show: doc => conf(
  title: [
    Antennas And Microwaves \
    Unit 2 - Exercises
  ],

  authors: (
    (
      name: "Benjamin Bresson",
      affiliation: "ETSETB",
      email: "benjamin.bresson@estudiantat.upc.edu",
    ),
  ),
  doc,
)




#set heading(numbering: "Q.1")

= \
We have a lightning that strikes between two clouds with a potential difference $U = V_B - V_A = 10^7 "V"$, with a peak current $I_p = 10^4 "A"$. Let us denote the rise time by $t_1$ and the fall time by $t_2$. We define the rise time by the duration it takes for the current to go from $0$ A to $I_p$ linearly. We define the fall time by the duration it takes for the current to go from $I_p$ to $0.5*I_p$ linearly.#footnote[according to this source : https://kingsmillindustries.com/characteristics-of-lightning-discharges/]. Here is the graph that denotes the evolution of the current : \

#figure(
  image("images/current_lightning.drawio.png", width: 75%),
  caption: [
    Plot of the approximated current in time. 
  ],
)
Therefore, we can express the current with two expressions so that : \
$I_1(t) = (I_p - 0)/(t_1 - 0) dot t = I_p/t_1 dot t = 10^9t$ #tab $forall t in [0;t_1]$\
$I_2(t) = (0.5I_p - I_p)/(t_2 - t_1) dot t 10^4 = -5 dot 10^7 (t - t_1) + 10^4$ #tab $forall t in [t_1;t_2]$. \
Using : $I(t) =  (d Q(t))/(d t)$, we can simply integrate $I(t)$ from 0 to $t_2$ to find the amount of Coulomb was carried considering $Q_("total") = Q_1 + Q_2$: \
$
Q_1 &= integral_0^t_1 I_1(t) d t 
\ &= integral_0^t_1 10^9 t d t 
\ &= [10^9/2 dot t^2 + 0]_0^t_1
\ Q_1 &= 0.05 C
$
We do the same for $Q_2$ : 
$
Q_2 &= integral_(t_1)^t_2 I_2(t) d t 
\ &= integral_(t_1)^t_2 -5 dot 10^7 (t - t_1) + 10^4 d t 
\ &= [-5 dot 10^7 (t_1^2/2 - t_1t) + 10^4t]_(t_1)^t_2
\ &= ...
\ Q_2 &= 0.7C
$
Finally, we find : $Q_"total" = 0.75C$.
\
Using the formula : $P = U dot I$ we find : $P_"peak" = U dot I_p = 10^7 dot 10^4 = 10^11 W = 100 G W$. \

The total energy is given by : $E-"tot" = integral_0^t_2 P(t)d t = integral_0^t_2 U dot I(t)d t = U dot Q_"total" = 7.5 dot 10^6 J = 7.5 M J$.

=
My mass : $m_"ben" = 65 k g $. The force applied by gravity on me is : $arrow(P) = m arrow(g) = m g hat(z)$. The norm of this force is : $P = m g = 65 dot 9.8 tilde.eq.rev 637 N$. As for the force applied by a charge onto another, both of +1C, its norm is : $F_r = k (1 dot 1)/(r^2) = 9 dot 10^9 (1)/(10^6) tilde.eq.rev 9000 N >> P$. \
My weight force and the force of repulsion are not similar : the latter is many orders of magnitude higher than my weight.

=
Because atoms are electrically neutral at a large scale.

=
The protons are held together by a force emanating from the strong interaction, this is a nuclear force. The strongest force is proton-neutron because there is no electrical repulsion, like proton-proton, and because there exists a stable bound.

=
Here is a drawing of the situation : \

#figure(
  image("images/qu5_unit_2_aam.png", width: 75%),
  caption: [
    Plot of the situation of the oscillating electron. 
  ],
)
The green circle is the observer, the red double arrow represent the oscillation of the electron, follow the equation : $z(t) = sin(omega t)$. There exists an angle $theta$. We can approximate it to be zero with following approach : \
using Pythagoras' theorem we find that : $tan(theta (t)) = (sin(omega t) dot 10^(-3))/(r) = (sin(2 pi f_(e^-) t) dot 10^(-3))/(10)$ \
This gives us the following expression for $theta (t)$ : \
$
theta (t) = arctan(sin(2 pi f_(e^-) t) dot 10^(-4))
$
But, we have the inside of the $arctan$ that is in $[-10^(-4);+10^(-4)]$ which is very close to zero, we approximate the $arctan$ by its argument, therefore : 
$
theta (t) = sin(2 pi f_(e^-) t) dot 10^(-4)
$
Finally, we find that $theta (t)$ is in $[-10^(-4);+10^(-4)]$ which is a very small angle. There fore, we approximate $theta (t)  tilde.eq.rev 0 tab forall t$.  \ 
Finally, with the approximation that $theta$ is extremely close to zero, we have the following field produced by the electron : 
$
arrow(E) &= (e)/(4 pi epsilon_0 r^2) hat(x)
\ &= 1.44 dot 10^(-11) V/m^-1
$
#pagebreak()
This document was written in Typst, a free and open source alternative to LaTeX. You can find the source code of this document in my Gitlab : https://gitlab.enssat.fr/bbresson/aam_unit_exercices_source_code