#let conf(
  title: none,
  authors: (),
  matiere: none,
  doc,
) = {
  // Set and show rules from before.


  // Set the logo and matiere
  // grid(
  //   columns: (1fr, 1fr),
  //   align(center, image("containers.jpg", height: 500pt)), 
  //   align(right, text(12pt, matiere)),
  // )
  
  set align(center)
  // image("../images/Enssat-UnivRennes_Noir.png", height: 50pt)

  //image("putsmthhere", height: 50pt)


  set align(horizon)
  line(length: 100%)

  //v(50pt)
  align(text(23pt,  title))
  //v(50pt)

  line(length: 100%)
  //v(50pt)
  let count = authors.len()
  let ncols = calc.min(count, 3)
  set align(top)

  place(
  bottom + center,
  grid(
    columns: (1fr,) * ncols,
    row-gutter: 24pt,
    ..authors.map(author => [
      #author.name \
      #author.affiliation \
      #link("mailto:" + author.email)
    ]),
  ),
)



  //pagebreak()
  set page(
  numbering: "1 / 1",
  )


  set align(left)
  doc
}